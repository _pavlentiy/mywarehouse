﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace myWarehouse
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(unit2.textBox2Processing("6", "8"), "10");
            Assert.AreEqual(unit2.textBox2Processing("350", "450"), "570,088");
        }

        [TestMethod]
        public void TestMethod2()
        {
            Assert.AreEqual(unit2.textBox2Processing("9999", "9999"), "14140,721");
            Assert.AreEqual(unit2.textBox2Processing("0", "0"), "0");
        }
    }
}
