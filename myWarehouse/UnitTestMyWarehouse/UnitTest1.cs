using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace myWarehouse
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Assert.AreEqual(unit1.textBox1Processing("3", "4"), "5");
            Assert.AreEqual(unit1.textBox1Processing("100", "200"), "223,607");
        }

        [TestMethod]
        public void TestMethod2()
        {
            Assert.AreEqual(unit1.textBox1Processing("9999", "9999"), "14140,721");
            Assert.AreEqual(unit1.textBox1Processing("0", "0"), "0");
        }
    }
}
