﻿using System;
using System.Collections.Generic;
using System.Text;

namespace myWarehouse
{
    public class unit2
    {
        public static string textBox2Processing(string str1, string str2)
        {
            double pif;
            int a, b;

            a = int.Parse(str1);
            b = int.Parse(str2);
            pif = Math.Sqrt((a * a) + (b * b));
            pif = Math.Round(pif, 3);

            return pif.ToString();
        }
    }
}
